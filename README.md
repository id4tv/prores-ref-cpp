# README #

ProRes decoding (WORK IN PROGRESS)

### NOTES ###

* C++ CPU implementation of ProRes decoder
* Input: bitstream encoded with ProRes, packed in an .mkv container
* Output: YUV decoded frame "tst_rec.yuv"

* Debug mode: ~470 ms per slice (1020 slices)
* Release mode: ~40 ms per slice (1020 slices)

### PREREQUISITES ###
