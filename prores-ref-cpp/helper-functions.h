#pragma once

#include <iostream>

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))

const int zigzag[64] = { 0, 1,  4,  5,  16, 17, 21, 22,
                         2, 3,  6,  7,  18, 20, 23, 28,
                         8, 9,  12, 13, 19, 24, 27, 29,
                        10, 11, 14, 15, 25, 26, 30, 31,
                        32, 33, 37, 38, 45, 46, 53, 54,
                        34, 36, 39, 44, 47, 52, 55, 60,
                        35, 40, 43, 48, 51, 56, 59, 61,
                        41, 42, 49, 50, 57, 58, 62, 63 };

const char* hex_char_to_bin(int c)
{
    //TODO: handle default/error
    switch (toupper(c))
    {
    case 0: return "0000";
    case 1: return "0001";
    case 2: return "0010";
    case 3: return "0011";
    case 4: return "0100";
    case 5: return "0101";
    case 6: return "0110";
    case 7: return "0111";
    case 8: return "1000";
    case 9: return "1001";
    case 10: return "1010";
    case 11: return "1011";
    case 12: return "1100";
    case 13: return "1101";
    case 14: return "1110";
    case 15: return "1111";
    }
}

int BinToDec8(char* BinIn) {

    int intOut = 0;
    intOut = intOut + (BinIn[0] - '0') * 128;
    //convert '0' or '1' to 0 or 1.
    intOut = intOut + (BinIn[1] - '0') * 64;
    intOut = intOut + (BinIn[2] - '0') * 32;
    intOut = intOut + (BinIn[3] - '0') * 16;
    intOut = intOut + (BinIn[4] - '0') * 8;
    intOut = intOut + (BinIn[5] - '0') * 4;
    intOut = intOut + (BinIn[6] - '0') * 2;
    intOut = intOut + (BinIn[7] - '0') * 1;
    return intOut;
}

int BinToDec16(char* BinIn) {

    int intOut = 0;
    intOut = intOut + (BinIn[0] - '0') * 32768;
    //convert '0' or '1' to 0 or 1.
    intOut = intOut + (BinIn[1] - '0') * 16384;
    intOut = intOut + (BinIn[2] - '0') * 8192;
    intOut = intOut + (BinIn[3] - '0') * 4096;
    intOut = intOut + (BinIn[4] - '0') * 2048;
    intOut = intOut + (BinIn[5] - '0') * 1024;
    intOut = intOut + (BinIn[6] - '0') * 512;
    intOut = intOut + (BinIn[7] - '0') * 256;
    intOut = intOut + (BinIn[8] - '0') * 128;
    intOut = intOut + (BinIn[9] - '0') * 64;
    intOut = intOut + (BinIn[10] - '0') * 32;
    intOut = intOut + (BinIn[11] - '0') * 16;
    intOut = intOut + (BinIn[12] - '0') * 8;
    intOut = intOut + (BinIn[13] - '0') * 4;
    intOut = intOut + (BinIn[14] - '0') * 2;
    intOut = intOut + (BinIn[15] - '0') * 1;
    return intOut;
}

/*int binaryToDecimal(unsigned char* n, const int size_of_n)
{
    int dec_value = 0;

    // Initializing base value to 1, i.e 2^0
    int base = 1;

    for (int i = size_of_n - 1; i >= 0; i--) {
        if (n[i] == '1')
            dec_value += base;
        base = base * 2;
    }
    return dec_value;
}*/