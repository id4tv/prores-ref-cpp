// prores-ref-cpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <chrono>

#include "helper-functions.h"
#include "entropy-decoding.h"
#include "prores-coeffs.h"
#include "idct.h"

#pragma warning(disable:6385)
#pragma warning(disable:6386)

#define M_MKV_METADATA_SIZE 712

int main()
{ 
    //-----READ BITSTREAM-----//
    FILE* fp = nullptr;
    int lSize;
    unsigned char* buffer, * bitstream, * codeword, * bitstring, * bitstream_full, * bitstream_slice;

    //open binary file
    fopen_s(&fp, std::string("../prores_frame100_1920x1080_10bit_422p.mkv").c_str(), "rb");
    if (fp == nullptr)
        return -1;

    //obtain file size
    fseek(fp, 0, SEEK_END);
    lSize = ftell(fp);
    rewind(fp);

    //allocate memory for buffers to contain the whole file and the bitstream
    buffer = (unsigned char*)malloc(sizeof(unsigned char) * lSize);
    bitstream = (unsigned char*)malloc(sizeof(unsigned char) * lSize - M_MKV_METADATA_SIZE);
    if (buffer == nullptr)
        return -1;
    if (bitstream == nullptr)
        return -1;

    //copy the file into the buffer
    fread(buffer, sizeof(unsigned char), lSize, fp);
    
    //close file
    fclose(fp);

    //-----SKIP MATROSKA METADATA-----//
    for (int i = 0; i < lSize - M_MKV_METADATA_SIZE; i++)
        bitstream[i] = buffer[M_MKV_METADATA_SIZE + i];

    //cleanup
    //free(buffer);
    
    //-----FRAME HEADER-----//
    uint16_t frame_header_size = bitstream[0] * 256 + bitstream[1];
    uint16_t width = bitstream[8] * 256 + bitstream[9];
    uint16_t height = bitstream[10] * 256 + bitstream[11];
    
    uint8_t chroma_format = bitstream[12] >> 6; // "2" - 422, "3" - 444
    uint8_t scan = bitstream[12] & 0x0f;        // "0" - progressive, "1" - interlaced top, "2" - interlaced bottom

    uint8_t color_primaries = bitstream[14];
    uint8_t transfer_function = bitstream[15];
    uint8_t color_matrix = bitstream[16];

    uint8_t source_pixel_format = bitstream[17] >> 4;
    uint8_t alpha_info = bitstream[17] & 0x0f;

    //TODO: this is an alternative way, normally should be two variables: custom_luma_quant_matrix_present and custom_chroma_quant_matrix_present
    uint8_t custom_quant_matrix_present = bitstream[19]; // "3" - custom luma + chroma, "2" - custom luma, "1" - custom chroma, "0" - no custom quant matrix 

    if (custom_quant_matrix_present == 3)
    {
        uint8_t QMatLuma[64], QMatChroma[64];
        for (int i = 0; i < 64; i++)
        {
            QMatLuma[i] = bitstream[20 + i];
            QMatChroma[i] = bitstream[20 + 64 + i];
        }
    }  

    //-----PICTURE HEADER-----//
    uint8_t picture_header_size = bitstream[148];
    uint32_t picture_data_size = (((bitstream[149]) * 256 + bitstream[150]) * 256 + bitstream[151]) * 256 + bitstream[152];
    uint16_t total_slices = bitstream[153] * 256 + bitstream[154];
    uint8_t slice_width = pow(2, bitstream[155] >> 4);
    uint8_t slice_height = pow(2, bitstream[155] & 0x0f);

    uint16_t slice_index_table[1020]; //TODO: use total_slices instead of 1020, need to declare it as constant
    
    for (int i = 0; i < total_slices*2; i+=2)
    {
        slice_index_table[i/2] = bitstream[156 + i] * 256 + bitstream[156 + i + 1];
    }

    //-----SLICE HEADER-----//
    //TODO: This is again hardcoded, in later version just try to get next byte
    int *slice_header_size = new int[1020];
    int *scale_factor = new int[1020];
    int *luma_data_size = new int[1020];
    int *u_data_size = new int[1020];
    
    slice_header_size[0] = bitstream[2196]; // 2196 = 156 + total_slices * 2
    scale_factor[0] = bitstream[2197];
    luma_data_size[0] = bitstream[2198] * 256 + bitstream[2199];
    u_data_size[0] = bitstream[2200] * 256 + bitstream[2201];

    //-----CODEWORD-----//
    codeword = (unsigned char*)malloc(sizeof(unsigned char) * lSize - M_MKV_METADATA_SIZE - 2201);
    for (int i = 0; i < lSize - M_MKV_METADATA_SIZE - 2201; i++)
        codeword[i] = bitstream[2202 + i];

    bitstring = (unsigned char*)malloc(sizeof(unsigned char) * (lSize - M_MKV_METADATA_SIZE - 2201) * 8);
    memcpy(bitstring, hex_char_to_bin(codeword[0] >> 4), 4);
    memcpy(bitstring + 4, hex_char_to_bin(codeword[0] & 0x0f), 4 + 1);

    int offset = 8;

    for (int i = 1; i < lSize - M_MKV_METADATA_SIZE - 2202; i++)
    {
        memcpy(bitstring + offset, hex_char_to_bin(codeword[i] >> 4), 4);
        memcpy(bitstring + offset + 4, hex_char_to_bin(codeword[i] & 0x0f), 4);
        offset = offset + 8;
    }

    //cleanup
    //free(bitstream);
    //free(codeword);

    //-----DC DECODING LUMA-----//
    qDC* m_qDC = new qDC;

    short *DC = (short*)malloc(sizeof(short) * 32);
    short *dc_diff = (short*)malloc(sizeof(short) * 32);

    long bs_org_size = strlen((char*)bitstring);
    //printf("BS length before DC decoding: %d", bs_org_size);

    bitstream_full = bitstring;

    bitstring = DC_decoding(bitstring, DC, dc_diff, 32);

    long bs_size_after_dc = strlen((char*)bitstring);
    //printf("\nBS length after DC decoding: %d\n", bs_size_after_dc);

    for (int i = 0; i < 32; i++)
    {
        m_qDC->Y[0][i] = DC[i];
    }

    //-----AC DECODING LUMA-----//
    qAC* m_qAC = new qAC;

    short *run = (short*)malloc(sizeof(short) * 2016);
    short *level = (short*)malloc(sizeof(short) * 2016);

    unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * luma_data_size[0] * 8 - (bs_org_size - bs_size_after_dc));
    for (int i = 0; i < luma_data_size[0] * 8 - (bs_org_size - bs_size_after_dc); i++)
        tmp[i] = bitstring[i];

    int idx = 0;
    bitstring = AC_decoding(tmp, run, level, idx);
    
    //-----REARRANGE ACs LUMA-----//
    int cnt = 0;
    for (int i = 0; i < idx; i++)
    {
        if (run[i] != 0)
        {
            for (int j = 0; j < run[i]; j++)
            {
                m_qAC->Y[0][cnt] = 0;
                cnt = cnt + 1;
            }
        }
        m_qAC->Y[0][cnt] = level[i];
        cnt = cnt + 1;
    }

    for (int i = cnt; i < 2016; i++)
        m_qAC->Y[0][i] = 0;

    //-----DC DECODING CHROMA U-----//
    short* DC_chroma = (short*)malloc(sizeof(short) * 16);
    short* dc_diff_chroma = (short*)malloc(sizeof(short) * 16);

    bitstring = &bitstream_full[luma_data_size[0] * 8];
    
    tmp = (unsigned char*)malloc(sizeof(unsigned char) * u_data_size[0] * 8);
    for (int i = 0; i < u_data_size[0] * 8; i++)
        tmp[i] = bitstring[i];

    bitstring = DC_decoding(tmp, DC_chroma, dc_diff_chroma, 16);

    for (int i = 0; i < 16; i++)
    {
        m_qDC->U[0][i] = DC_chroma[i];
    }

    //-----AC DECODING CHROMA U-----//
    short* run_chroma = (short*)malloc(sizeof(short) * 1008);
    short* level_chroma = (short*)malloc(sizeof(short) * 1008);

    idx = 0;
    bitstring = AC_decoding(bitstring, run_chroma, level_chroma, idx);

    //-----REARRANGE ACs CHROMA-----//
    cnt = 0;
    for (int i = 0; i < idx; i++)
    {
        if (run_chroma[i] != 0)
        {
            for (int j = 0; j < run_chroma[i]; j++)
            {
                m_qAC->U[0][cnt] = 0;
                cnt = cnt + 1;
            }
        }
        m_qAC->U[0][cnt] = level_chroma[i];
        cnt = cnt + 1;
    }

    for (int i = cnt; i < 1008; i++)
        m_qAC->U[0][i] = 0;

    //-----DC DECODING CHROMA V-----//
    int *v_data_size = new int[1020];
    v_data_size[0] = slice_index_table[0] - luma_data_size[0] - u_data_size[0] - (slice_header_size[0] / 8);

    bitstring = &bitstream_full[(luma_data_size[0] + u_data_size[0]) * 8];

    tmp = (unsigned char*)malloc(sizeof(unsigned char) * v_data_size[0] * 8);
    for (int i = 0; i < v_data_size[0] * 8; i++)
        tmp[i] = bitstring[i];

    bitstring = DC_decoding(tmp, DC_chroma, dc_diff_chroma, 16);

    for (int i = 0; i < 16; i++)
    {
        m_qDC->V[0][i] = DC_chroma[i];
    }

    //-----AC DECODING CHROMA V-----//
    idx = 0;
    bitstring = AC_decoding(bitstring, run_chroma, level_chroma, idx);

    //-----REARRANGE ACs CHROMA-----//
    cnt = 0;
    for (int i = 0; i < idx; i++)
    {
        if (run_chroma[i] != 0)
        {
            for (int j = 0; j < run_chroma[i]; j++)
            {
                m_qAC->V[0][cnt] = 0;
                cnt = cnt + 1;
            }
        }
        m_qAC->V[0][cnt] = level_chroma[i];
        cnt = cnt + 1;
    }

    for (int i = cnt; i < 1008; i++)
        m_qAC->V[0][i] = 0;
    
    //-----DECODE ALL DCs and ACs-----//
    uint64_t sum_of_slice_header_size, sum_of_luma_data_size, sum_of_u_data_size, sum_of_v_data_size;
    
    //measure time
    auto start_time = std::chrono::high_resolution_clock::now();
    auto start_time_entropy = std::chrono::high_resolution_clock::now();

    for (int slice = 1; slice < 1020; slice++)
    { 
        printf("SLICE: %d OK \n", slice);
        //auto tm_tst_headers = std::chrono::high_resolution_clock::now();
        
        sum_of_slice_header_size = 0;
        sum_of_luma_data_size = 0;
        sum_of_u_data_size = 0;
        sum_of_v_data_size = 0;
        
        for (int i = 1; i < slice; i++)
            sum_of_slice_header_size += slice_header_size[i];

        for (int i = 0; i < slice; i++)
        {
            sum_of_luma_data_size += luma_data_size[i];
            sum_of_u_data_size += u_data_size[i];
            sum_of_v_data_size += v_data_size[i];
        }

        bitstream_slice = &bitstream_full[sum_of_slice_header_size + (sum_of_luma_data_size + sum_of_u_data_size + sum_of_v_data_size) * 8];

        bitstream = bitstream_slice;

        //Read slice headers
        char binary8[9];
        char binary16[17];
        for (int i = 0; i < 8; i++)
            binary8[i] = bitstream[i];
        slice_header_size[slice] = BinToDec8(binary8);

        for (int i = 0; i < 8; i++)
            binary8[i] = bitstream[i + 8];
        scale_factor[slice] = BinToDec8(binary8);
        
        for (int i = 0; i < 16; i++)
            binary16[i] = bitstream[i + 16];
        luma_data_size[slice] = BinToDec16(binary16);

        for (int i = 0; i < 16; i++)
            binary16[i] = bitstream[i + 32];
        u_data_size[slice] = BinToDec16(binary16);
        
        bitstream = &bitstream[48];

        //auto tm_tst_headers_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Read headers in: " << (tm_tst_headers_end - tm_tst_headers) / std::chrono::milliseconds(1) << "ms)\n";

        //DC Y

        //auto tm_tst_DC_Y = std::chrono::high_resolution_clock::now();

        bs_org_size = strlen((char*)bitstream);

        bitstream = DC_decoding(bitstream, DC, dc_diff, 32);

        bs_size_after_dc = strlen((char*)bitstream);

        for (int i = 0; i < 32; i++)
            m_qDC->Y[slice][i] = DC[i];

        //auto tm_tst_DC_Y_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded Y DCs in: " << (tm_tst_DC_Y_end - tm_tst_DC_Y) / std::chrono::milliseconds(1) << "ms)\n";

        //AC Y

        //auto tm_tst_AC_Y = std::chrono::high_resolution_clock::now();

        tmp = (unsigned char*)malloc(sizeof(unsigned char) * luma_data_size[slice] * 8 - (bs_org_size - bs_size_after_dc));
        for (int i = 0; i < luma_data_size[slice] * 8 - (bs_org_size - bs_size_after_dc); i++)
            tmp[i] = bitstream[i];

        idx = 0;
        //printf("      AC Y--\n");
        bitstream = AC_decoding(tmp, run, level, idx);

        //auto tm_tst_AC_Y_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded Y ACs in: " << (tm_tst_AC_Y_end - tm_tst_AC_Y) / std::chrono::milliseconds(1) << "ms)\n";
        //std::cout << "    [idx = " << idx << "]\n";

        //-----REARRANGE ACs LUMA-----//

        //auto tm_tst_AC_rec = std::chrono::high_resolution_clock::now();

        cnt = 0;
        for (int i = 0; i < idx; i++)
        {
            if (run[i] != 0)
            {
                for (int j = 0; j < run[i]; j++)
                {
                    m_qAC->Y[slice][cnt] = 0;
                    cnt = cnt + 1;
                }
            }
            m_qAC->Y[slice][cnt] = level[i];
            cnt = cnt + 1;
        }

        for (int i = cnt; i < 2016; i++)
            m_qAC->Y[slice][i] = 0;

        //auto tm_tst_AC_rec_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Rearranged Y ACs in: " << (tm_tst_AC_rec_end - tm_tst_AC_rec) / std::chrono::milliseconds(1) << "ms)\n";

        //DC U

        //auto tm_tst_DC_U = std::chrono::high_resolution_clock::now();

        bitstream = &bitstream_slice[slice_header_size[slice] + luma_data_size[slice] * 8];
        
        tmp = (unsigned char*)malloc(sizeof(unsigned char) * u_data_size[slice] * 8);
        for (int i = 0; i < u_data_size[slice] * 8; i++)
            tmp[i] = bitstream[i];

        bitstream = DC_decoding(tmp, DC_chroma, dc_diff_chroma, 16);

        for (int i = 0; i < 16; i++)
            m_qDC->U[slice][i] = DC_chroma[i];

        //auto tm_tst_DC_U_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded U DCs in: " << (tm_tst_DC_U_end - tm_tst_DC_U) / std::chrono::milliseconds(1) << "ms)\n";

        //AC U

        //auto tm_tst_AC_U = std::chrono::high_resolution_clock::now();

        idx = 0;
        //printf("      AC U--\n");
        bitstream = AC_decoding(bitstream, run_chroma, level_chroma, idx);

        //auto tm_tst_AC_U_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded U ACs in: " << (tm_tst_AC_U_end - tm_tst_AC_U) / std::chrono::milliseconds(1) << "ms)\n";
        //std::cout << "    [idx = " << idx << "]\n";

        //-----REARRANGE ACs CHROMA-----//

        //auto tm_tst_AC_rec_U = std::chrono::high_resolution_clock::now();

        cnt = 0;
        for (int i = 0; i < idx; i++)
        {
            if (run_chroma[i] != 0)
            {
                for (int j = 0; j < run_chroma[i]; j++)
                {
                    m_qAC->U[slice][cnt] = 0;
                    cnt = cnt + 1;
                }
            }
            m_qAC->U[slice][cnt] = level_chroma[i];
            cnt = cnt + 1;
        }

        for (int i = cnt; i < 1008; i++)
            m_qAC->U[slice][i] = 0;

        //auto tm_tst_AC_rec_U_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Rearranged U ACs in: " << (tm_tst_AC_rec_U_end - tm_tst_AC_rec_U) / std::chrono::milliseconds(1) << "ms)\n";

        //DC V

        //auto tm_tst_DC_V = std::chrono::high_resolution_clock::now();

        v_data_size[slice] = slice_index_table[slice] - luma_data_size[slice] - u_data_size[slice] - (slice_header_size[slice] / 8);

        bitstream = &bitstream_slice[slice_header_size[slice] + (luma_data_size[slice] + u_data_size[slice]) * 8];

        tmp = (unsigned char*)malloc(sizeof(unsigned char) * v_data_size[slice] * 8);
        for (int i = 0; i < v_data_size[slice] * 8; i++)
            tmp[i] = bitstream[i];

        bitstream = DC_decoding(tmp, DC_chroma, dc_diff_chroma, 16);

        for (int i = 0; i < 16; i++)
            m_qDC->V[slice][i] = DC_chroma[i];

        //auto tm_tst_DC_V_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded V DCs in: " << (tm_tst_DC_V_end - tm_tst_DC_V) / std::chrono::milliseconds(1) << "ms)\n";

        //AC V

        //auto tm_tst_AC_V = std::chrono::high_resolution_clock::now();

        idx = 0;
        //printf("      AC V--\n");
        bitstream = AC_decoding(bitstream, run_chroma, level_chroma, idx);

        //auto tm_tst_AC_V_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Decoded V ACs in: " << (tm_tst_AC_V_end - tm_tst_AC_V) / std::chrono::milliseconds(1) << "ms)\n";
        //std::cout << "    [idx = " << idx << "]\n";

        //-----REARRANGE ACs CHROMA-----//

        //auto tm_tst_AC_rec_V = std::chrono::high_resolution_clock::now();

        cnt = 0;
        for (int i = 0; i < idx; i++)
        {
            if (run_chroma[i] != 0)
            {
                for (int j = 0; j < run_chroma[i]; j++)
                {
                    m_qAC->V[slice][cnt] = 0;
                    cnt = cnt + 1;
                }
            }
            m_qAC->V[slice][cnt] = level_chroma[i];
            cnt = cnt + 1;
        }

        for (int i = cnt; i < 1008; i++)
            m_qAC->V[slice][i] = 0;

        //auto tm_tst_AC_rec_V_end = std::chrono::high_resolution_clock::now();
        //std::cout << "  (Rearranged V ACs in: " << (tm_tst_AC_rec_V_end - tm_tst_AC_rec_V) / std::chrono::milliseconds(1) << "ms)\n";

    }

    auto end_time_entropy = std::chrono::high_resolution_clock::now();

    //-----ZIG ZAG SCANNING + INVERSE QUANTIZATION-----//

    auto start_time_zigzag = std::chrono::high_resolution_clock::now();

    qF* m_qF = new qF;
    Coeffs* m_qCoeffs_tmp = new Coeffs;
    Coeffs* m_qCoeffs = new Coeffs;
    
    //Merge Y DC + AC luma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 32; j++)
            m_qF->Y[i][j] = m_qDC->Y[i][j];

        for (int k = 32; k < 2048; k++)
            m_qF->Y[i][k] = m_qAC->Y[i][k - 32];
    }

    //Merge Y DC + AC chroma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            m_qF->U[i][j] = m_qDC->U[i][j];
            m_qF->V[i][j] = m_qDC->V[i][j];
        }

        for (int k = 16; k < 1024; k++)
        {
            m_qF->U[i][k] = m_qAC->U[i][k - 16];
            m_qF->V[i][k] = m_qAC->V[i][k - 16];
        }
    }

    //cleanup
    //free(m_qDC);
    //free(m_qAC);

    //Rearrange DC + AC luma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 32; j++)
        {
            cnt = 0;

            for (int k = 0; k < 64; k++)
            {
                m_qCoeffs_tmp->Y[i][j][k] = m_qF->Y[i][cnt + j];
                cnt = cnt + 32;
            }

        }
    }

    //Rearrange DC + AC chroma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            cnt = 0;

            for (int k = 0; k < 64; k++)
            {
                m_qCoeffs_tmp->U[i][j][k] = m_qF->U[i][cnt + j];
                m_qCoeffs_tmp->V[i][j][k] = m_qF->V[i][cnt + j];
                cnt = cnt + 16;
            }

        }
    }

    //Zig-zag scanning + inverse quantization luma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 32; j++)
        {
            for (int k = 0; k < 64; k++)
                m_qCoeffs->Y[i][j][k] = m_qCoeffs_tmp->Y[i][j][zigzag[k]] * scale_factor[i];
        }
    }

    //Zig-zag scanning  + inverse quantization chroma
    for (int i = 0; i < 1020; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            for (int k = 0; k < 64; k++)
            {
                m_qCoeffs->U[i][j][k] = m_qCoeffs_tmp->U[i][j][zigzag[k]] * scale_factor[i];
                m_qCoeffs->V[i][j][k] = m_qCoeffs_tmp->V[i][j][zigzag[k]] * scale_factor[i];
            }
        }
    }
   
    //cleanup
    //free(m_qF);
    //free(m_qCoeffs_tmp);

    auto end_time_zigzag = std::chrono::high_resolution_clock::now();

    //-----INVERSE DCT-----//

    auto start_time_dct = std::chrono::high_resolution_clock::now();

    Coeffs* m_pixels = new Coeffs;

    //DCT-Y
    for (int slices = 0; slices < 1020; slices++)
    {
        for (int blocks = 0; blocks < 32; blocks++)
        {
            float data[64] = { 0 };
            for (int j = 0; j < 64; j++)
                data[j] = m_qCoeffs->Y[slices][blocks][j];

            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i * 8], 1);	//process rows
            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i], 8);	    //process columns

            for (int l = 0; l < 64; l++)
                m_pixels->Y[slices][blocks][l] = round(data[l]);
        }
    }

    //DCT-U
    for (int slices = 0; slices < 1020; slices++)
    {
        for (int blocks = 0; blocks < 16; blocks++)
        {
            float data[64] = { 0 };
            for (int j = 0; j < 64; j++)
                data[j] = m_qCoeffs->U[slices][blocks][j];

            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i * 8], 1);	//process rows
            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i], 8);	    //process columns

            for (int l = 0; l < 64; l++)
                m_pixels->U[slices][blocks][l] = round(data[l]);
        }
    }

    //DCT-V
    for (int slices = 0; slices < 1020; slices++)
    {
        for (int blocks = 0; blocks < 16; blocks++)
        {
            float data[64] = { 0 };
            for (int j = 0; j < 64; j++)
                data[j] = m_qCoeffs->V[slices][blocks][j];

            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i * 8], 1);	//process rows
            for (int i = 0; i < 8; i++)
                InplaceIDCTvector(&data[i], 8);	    //process columns

            for (int l = 0; l < 64; l++)
                m_pixels->V[slices][blocks][l] = round(data[l]);
        }
    }

    auto end_time_dct = std::chrono::high_resolution_clock::now();

    //Visualize selected 8x8 blocks
    /*printf("\n");
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%d, ", m_pixels->Y[1019][31][i * 8 + j]);
        }
        printf("\n");
    }

    printf("\n");
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%d, ", m_pixels->U[1019][15][i * 8 + j]);
        }
        printf("\n");
    }

    printf("\n");
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            printf("%d, ", m_pixels->V[1019][15][i * 8 + j]);
        }
        printf("\n");
    }*/

    //-----RE-CONSTRUCT MACROBLOCKS-----//

    auto start_time_rec = std::chrono::high_resolution_clock::now();

    Macroblock* m_MB = new Macroblock;

    for (int s = 0; s < 1020; s++)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                //Re-construct Y MBs
                m_MB->Y[s][i][0][j] = m_pixels->Y[s][i * 4][j];
                m_MB->Y[s][i][1][j] = m_pixels->Y[s][i * 4][j + 8];
                m_MB->Y[s][i][2][j] = m_pixels->Y[s][i * 4][j + 16];
                m_MB->Y[s][i][3][j] = m_pixels->Y[s][i * 4][j + 24];
                m_MB->Y[s][i][4][j] = m_pixels->Y[s][i * 4][j + 32];
                m_MB->Y[s][i][5][j] = m_pixels->Y[s][i * 4][j + 40];
                m_MB->Y[s][i][6][j] = m_pixels->Y[s][i * 4][j + 48];
                m_MB->Y[s][i][7][j] = m_pixels->Y[s][i * 4][j + 56];

                m_MB->Y[s][i][0][j + 8] = m_pixels->Y[s][i * 4 + 1][j];
                m_MB->Y[s][i][1][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 8];
                m_MB->Y[s][i][2][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 16];
                m_MB->Y[s][i][3][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 24];
                m_MB->Y[s][i][4][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 32];
                m_MB->Y[s][i][5][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 40];
                m_MB->Y[s][i][6][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 48];
                m_MB->Y[s][i][7][j + 8] = m_pixels->Y[s][i * 4 + 1][j + 56];

                m_MB->Y[s][i][8][j]  = m_pixels->Y[s][i * 4 + 2][j];
                m_MB->Y[s][i][9][j]  = m_pixels->Y[s][i * 4 + 2][j + 8];
                m_MB->Y[s][i][10][j] = m_pixels->Y[s][i * 4 + 2][j + 16];
                m_MB->Y[s][i][11][j] = m_pixels->Y[s][i * 4 + 2][j + 24];
                m_MB->Y[s][i][12][j] = m_pixels->Y[s][i * 4 + 2][j + 32];
                m_MB->Y[s][i][13][j] = m_pixels->Y[s][i * 4 + 2][j + 40];
                m_MB->Y[s][i][14][j] = m_pixels->Y[s][i * 4 + 2][j + 48];
                m_MB->Y[s][i][15][j] = m_pixels->Y[s][i * 4 + 2][j + 56];

                m_MB->Y[s][i][8][j + 8]  = m_pixels->Y[s][i * 4 + 3][j];
                m_MB->Y[s][i][9][j + 8]  = m_pixels->Y[s][i * 4 + 3][j + 8];
                m_MB->Y[s][i][10][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 16];
                m_MB->Y[s][i][11][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 24];
                m_MB->Y[s][i][12][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 32];
                m_MB->Y[s][i][13][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 40];
                m_MB->Y[s][i][14][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 48];
                m_MB->Y[s][i][15][j + 8] = m_pixels->Y[s][i * 4 + 3][j + 56];

                //Re-construct U MBs
                m_MB->U[s][i][0][j] = m_pixels->U[s][i * 2][j];
                m_MB->U[s][i][1][j] = m_pixels->U[s][i * 2][j + 8];
                m_MB->U[s][i][2][j] = m_pixels->U[s][i * 2][j + 16];
                m_MB->U[s][i][3][j] = m_pixels->U[s][i * 2][j + 24];
                m_MB->U[s][i][4][j] = m_pixels->U[s][i * 2][j + 32];
                m_MB->U[s][i][5][j] = m_pixels->U[s][i * 2][j + 40];
                m_MB->U[s][i][6][j] = m_pixels->U[s][i * 2][j + 48];
                m_MB->U[s][i][7][j] = m_pixels->U[s][i * 2][j + 56];

                m_MB->U[s][i][8][j] = m_pixels->U[s][i * 2 + 1][j];
                m_MB->U[s][i][9][j] = m_pixels->U[s][i * 2 + 1][j + 8];
                m_MB->U[s][i][10][j] = m_pixels->U[s][i * 2 + 1][j + 16];
                m_MB->U[s][i][11][j] = m_pixels->U[s][i * 2 + 1][j + 24];
                m_MB->U[s][i][12][j] = m_pixels->U[s][i * 2 + 1][j + 32];
                m_MB->U[s][i][13][j] = m_pixels->U[s][i * 2 + 1][j + 40];
                m_MB->U[s][i][14][j] = m_pixels->U[s][i * 2 + 1][j + 48];
                m_MB->U[s][i][15][j] = m_pixels->U[s][i * 2 + 1][j + 56];

                //Re-construct V MBs
                m_MB->V[s][i][0][j] = m_pixels->V[s][i * 2][j];
                m_MB->V[s][i][1][j] = m_pixels->V[s][i * 2][j + 8];
                m_MB->V[s][i][2][j] = m_pixels->V[s][i * 2][j + 16];
                m_MB->V[s][i][3][j] = m_pixels->V[s][i * 2][j + 24];
                m_MB->V[s][i][4][j] = m_pixels->V[s][i * 2][j + 32];
                m_MB->V[s][i][5][j] = m_pixels->V[s][i * 2][j + 40];
                m_MB->V[s][i][6][j] = m_pixels->V[s][i * 2][j + 48];
                m_MB->V[s][i][7][j] = m_pixels->V[s][i * 2][j + 56];

                m_MB->V[s][i][8][j] = m_pixels->V[s][i * 2 + 1][j];
                m_MB->V[s][i][9][j] = m_pixels->V[s][i * 2 + 1][j + 8];
                m_MB->V[s][i][10][j] = m_pixels->V[s][i * 2 + 1][j + 16];
                m_MB->V[s][i][11][j] = m_pixels->V[s][i * 2 + 1][j + 24];
                m_MB->V[s][i][12][j] = m_pixels->V[s][i * 2 + 1][j + 32];
                m_MB->V[s][i][13][j] = m_pixels->V[s][i * 2 + 1][j + 40];
                m_MB->V[s][i][14][j] = m_pixels->V[s][i * 2 + 1][j + 48];
                m_MB->V[s][i][15][j] = m_pixels->V[s][i * 2 + 1][j + 56];
            }
        }
    }

    //cleanup
    //free(m_pixels);

    //Visualize selected 16x16 luma MBs and 16x8 chroma MBs
   /* printf("\n");
    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            printf("%d, ", m_MB->Y[0][0][i][j]);
        }
        printf("\n");
    }

    printf("\n");
    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            printf("%d, ", m_MB->Y[0][1][i][j]);
        }
        printf("\n");
    }

    printf("\n");
    for (int i = 0; i < 16; i++)
    {
        for (int j = 0; j < 16; j++)
        {
            printf("%d, ", m_MB->Y[0][2][i][j]);
        }
        printf("\n");
    }*/

    //-----RE-CONSTRUCT MACROBLOCKS ROWS-----//
    slice_tmp* m_tmp = new slice_tmp;
    MB_row* m_MB_row = new MB_row;

    //Re-construct luma MB rows
    for (int row = 0; row < 68; row++)
    {
        for (int l = 0; l < 15; l++)
        {
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 16; j++)
                {
                    m_tmp->Y[l][i][j] = m_MB->Y[l + (15 * row)][0][i][j];
                    m_tmp->Y[l][i][j + 16] = m_MB->Y[l + (15 * row)][1][i][j];
                    m_tmp->Y[l][i][j + 32] = m_MB->Y[l + (15 * row)][2][i][j];
                    m_tmp->Y[l][i][j + 48] = m_MB->Y[l + (15 * row)][3][i][j];
                    m_tmp->Y[l][i][j + 64] = m_MB->Y[l + (15 * row)][4][i][j];
                    m_tmp->Y[l][i][j + 80] = m_MB->Y[l + (15 * row)][5][i][j];
                    m_tmp->Y[l][i][j + 96] = m_MB->Y[l + (15 * row)][6][i][j];
                    m_tmp->Y[l][i][j + 112] = m_MB->Y[l + (15 * row)][7][i][j];
                }
            }
        }
        for (int ii = 0; ii < 16; ii++)
        {
            for (int kk = 0; kk < 15; kk++)
            {
                for (int jj = 0; jj < 128; jj++)
                {
                    m_MB_row->Y[row][ii][jj + (kk * 128)] = max(0x40, min(0x3AC, m_tmp->Y[kk][ii][jj] + 512));
                    //m_MB_row->Y[row][ii][jj + (kk * 128)] = m_tmp->Y[kk][ii][jj] + 512;
                }
            }
        }
    }

    //Re-construct chroma MB rows
    for (int row = 0; row < 68; row++)
    {
        for (int l = 0; l < 15; l++)
        {
            for (int i = 0; i < 16; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    m_tmp->U[l][i][j] = m_MB->U[l + (15 * row)][0][i][j];
                    m_tmp->U[l][i][j + 8] = m_MB->U[l + (15 * row)][1][i][j];
                    m_tmp->U[l][i][j + 16] = m_MB->U[l + (15 * row)][2][i][j];
                    m_tmp->U[l][i][j + 24] = m_MB->U[l + (15 * row)][3][i][j];
                    m_tmp->U[l][i][j + 32] = m_MB->U[l + (15 * row)][4][i][j];
                    m_tmp->U[l][i][j + 40] = m_MB->U[l + (15 * row)][5][i][j];
                    m_tmp->U[l][i][j + 48] = m_MB->U[l + (15 * row)][6][i][j];
                    m_tmp->U[l][i][j + 56] = m_MB->U[l + (15 * row)][7][i][j];

                    m_tmp->V[l][i][j] = m_MB->V[l + (15 * row)][0][i][j];
                    m_tmp->V[l][i][j + 8] = m_MB->V[l + (15 * row)][1][i][j];
                    m_tmp->V[l][i][j + 16] = m_MB->V[l + (15 * row)][2][i][j];
                    m_tmp->V[l][i][j + 24] = m_MB->V[l + (15 * row)][3][i][j];
                    m_tmp->V[l][i][j + 32] = m_MB->V[l + (15 * row)][4][i][j];
                    m_tmp->V[l][i][j + 40] = m_MB->V[l + (15 * row)][5][i][j];
                    m_tmp->V[l][i][j + 48] = m_MB->V[l + (15 * row)][6][i][j];
                    m_tmp->V[l][i][j + 56] = m_MB->V[l + (15 * row)][7][i][j];
                }
            }
        }
        for (int ii = 0; ii < 16; ii++)
        {
            for (int kk = 0; kk < 15; kk++)
            {
                for (int jj = 0; jj < 64; jj++)
                {
                    m_MB_row->U[row][ii][jj + (kk * 64)] = max(0x40, min(0x3AC, m_tmp->U[kk][ii][jj] + 512));
                    m_MB_row->V[row][ii][jj + (kk * 64)] = max(0x40, min(0x3AC, m_tmp->V[kk][ii][jj] + 512));
                    //m_MB_row->U[row][ii][jj + (kk * 64)] = m_tmp->U[kk][ii][jj] + 512;
                    //m_MB_row->V[row][ii][jj + (kk * 64)] = m_tmp->V[kk][ii][jj] + 512;
                }
            }
        }
    }

    auto end_time_rec = std::chrono::high_resolution_clock::now();

    //Save to file
    fopen_s(&fp, std::string("tst_rec.yuv").c_str(), "ab");
    if (fp == nullptr)
        return -1;

    fwrite(m_MB_row->Y, sizeof(m_MB_row->Y), 1, fp);
    fwrite(m_MB_row->U, sizeof(m_MB_row->U), 1, fp);
    fwrite(m_MB_row->V, sizeof(m_MB_row->V), 1, fp);
    fclose(fp);

    //cleanup
    //free(m_MB_row);

    //calculate time
    auto end_time = std::chrono::high_resolution_clock::now();
    auto time = end_time - start_time;
    auto time_entropy = end_time_entropy - start_time_entropy;
    auto time_zigzag = end_time_zigzag - start_time_zigzag;
    auto time_dct = end_time_dct - start_time_dct;
    auto time_rec = end_time_rec - start_time_rec;

    std::cout << "\nTest decoder took " << time / std::chrono::milliseconds(1) << "ms in total\n";
    std::cout << "or " << time / std::chrono::milliseconds(1) / 1000 / 60 << "mins in total\n";
    std::cout << "(approx. " << time / std::chrono::milliseconds(1) / 1020 << "ms per slice)\n";

    std::cout << "\n(entropy decoding took: " << time_entropy / std::chrono::milliseconds(1) << "ms)\n";
    std::cout << "(zigzag scanning took: " << time_zigzag / std::chrono::milliseconds(1) << "ms)\n";
    std::cout << "(inverse dct took: " << time_dct / std::chrono::milliseconds(1) << "ms)\n";
    std::cout << "(MB reconstruction took: " << time_rec / std::chrono::milliseconds(1) << "ms)\n";

    //terminate
    return 0;
}
