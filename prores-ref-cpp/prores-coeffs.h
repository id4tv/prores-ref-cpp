#pragma once

#include <cstdint>

struct qDC
{
	int16_t Y[1020][32]; //32 Y DCs per slice, 1020 slices
	int16_t U[1020][16]; //16 U DCs per slice, 1020 slices
	int16_t V[1020][16]; //16 V Dcs per slice, 1020 slices
};

struct qAC
{
	int16_t Y[1020][2016]; //2016 Y ACs per slice, 1020 slices
	int16_t U[1020][1008]; //1008 U ACs per slice, 1020 slices
	int16_t V[1020][1008]; //1008 V ACS per slice, 1020 slices
};

struct qF
{
	int16_t Y[1020][2048]; //2048 qcoeffs for Y
	int16_t U[1020][1024]; //1024 qcoeffs for U
	int16_t V[1020][1024]; //1024 qcoeffs for V
};

struct Coeffs
{
	int16_t Y[1020][32][64]; //64 coeffs per block, 32 Y blocks per slice, 1020 slices
	int16_t U[1020][16][64]; //64 coeffs per block, 16 U blocks per slice, 1020 slices
	int16_t V[1020][16][64]; //64 coeffs per block, 16 V blocks per slice, 1020 slices
};

struct Macroblock
{
	int16_t Y[1020][8][16][16]; //16x16 pixels per MB, 8 MBs per slice, 1020 slices
	int16_t U[1020][8][16][8];  //16x8 pixels per MB, 8 MBs per slice, 1020 slices (422)
	int16_t V[1020][8][16][8];  //16x8 pixels per MB, 8 MBs per slice, 1020 slices (422)
};

struct slice_tmp
{
	int16_t Y[15][16][128];
	int16_t U[15][16][64];
	int16_t V[15][16][64];
};

struct MB_row
{
	int16_t Y[68][16][1920];
	int16_t U[68][16][960];
	int16_t V[68][16][960];
};
