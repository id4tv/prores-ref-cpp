#pragma once

auto define_codebook_run(uint8_t previousRun)
{
    struct codebook_run
    {
        int lastRiceQ;
        int Krice;
        int Kexp;
        std::string codebook;
    };

    previousRun = abs(previousRun);

    if (previousRun == 0 || previousRun == 1)
        return codebook_run{ 2, 0, 1, "combo" };
    else if (previousRun == 2 || previousRun == 3)
        return codebook_run{ 1, 0, 1, "combo" };
    else if (previousRun == 4)
        return codebook_run{ 0, 0, 0, "exp" };
    else if (previousRun == 5 || previousRun == 6 || previousRun == 7 || previousRun == 8)
        return codebook_run{ 1, 1, 2, "combo" };
    else if (previousRun == 9 || previousRun == 10 || previousRun == 11 || previousRun == 12 || previousRun == 13 || previousRun == 14)
        return codebook_run{ 0, 0, 1, "exp" };
    else if (previousRun >= 15)
        return codebook_run{ 0, 0, 2, "exp" };
    else
        printf("Error: previousRun cannot be negative"); exit(1);
}

auto define_codebook_level(uint8_t previousLevel)
{
    struct codebook_level
    {
        int lastRiceQ;
        int Krice;
        int Kexp;
        std::string codebook;
    };

    if (previousLevel == 0)
        return codebook_level{ 2, 0, 2, "combo" };
    else if (previousLevel == 1)
        return codebook_level{ 1, 0, 1, "combo" };
    else if (previousLevel == 2)
        return codebook_level{ 2, 0, 1, "combo" };
    else if (previousLevel == 3)
        return codebook_level{ 0, 0, 0, "exp" };
    else if (previousLevel == 4)
        return codebook_level{ 0, 0, 1, "exp" };
    else if (previousLevel == 5)
        return codebook_level{ 0, 0, 1, "exp" };
    else if (previousLevel == 6)
        return codebook_level{ 0, 0, 1, "exp" };
    else if (previousLevel == 7)
        return codebook_level{ 0, 0, 1, "exp" };
    else if (previousLevel >= 8)
        return codebook_level{ 0, 0, 2, "exp" };
    else
        printf("Error: previousLevel cannot be negative"); exit(1);
}

int Inverse_signed_golomb_combo_codes(const int n)
{
    if (n % 2 == 0)
        return n / 2;
    else
        return -((n + 1) / 2);
}

int Golomb_decoding(unsigned char* bitstring, const int k, const int codeword_size)
{
    int q = 0;
    int n = 0;

    for (int i = 0; i < codeword_size; i++)
    {
        if (bitstring[i] == '0')
            q = q + 1;
        else if (bitstring[i] == '1')
            break;
    }

    if (codeword_size - k != 0)
    {
        n = q * pow(2, k) + std::strtoull(reinterpret_cast<char*>(&bitstring[codeword_size - k]), 0, 2);
    }
    else
    {
        n = q * pow(2, k);
    }

    return n;
}

int Exp_golomb_decoding(unsigned char* bitstring, const int k)
{
    return std::strtoull(reinterpret_cast<char*>(bitstring), 0, 2) - pow(2, k);
}

unsigned char* DC_decoding(unsigned char* bitstring, short* DC, short* dc_diff, int num_coeffs)
{
    int k = 0;
    
    for (int coeff = 0; coeff < num_coeffs; coeff++)
    {
        int q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
        int codeword_size, dec_value;

        if (coeff == 0)
            k = 5;
        else if (coeff == 1)
            k = 3;

        if (k == 2)
        {
            if (q <= 1)
            {
                codeword_size = q + 1 + 2;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                dec_value = Golomb_decoding(tmp, 2, codeword_size);
                dc_diff[coeff] = Inverse_signed_golomb_combo_codes(dec_value);
            }
            else
            {
                bitstring = &bitstring[2];
                q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
                codeword_size = 2 * q + 1 + 3;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                int n = Exp_golomb_decoding(tmp, 3);
                dec_value = n + (2 * pow(2, 2));
                dc_diff[coeff] = Inverse_signed_golomb_combo_codes(dec_value);
            }
        }
        else
        {
            codeword_size = 2 * q + 1 + k;
            
            unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
            for (int i = 0; i < codeword_size; i++)
                tmp[i] = bitstring[i];
            
            dec_value = Exp_golomb_decoding(tmp, k);
            dc_diff[coeff] = Inverse_signed_golomb_combo_codes(dec_value);
        }
        if (coeff < 1)
        {
            DC[coeff] = dc_diff[coeff];
        }
        else
        {
            if (coeff != 1 && dc_diff[coeff - 1] < 0)
            {
                dc_diff[coeff] = -dc_diff[coeff];
            }

            DC[coeff] = DC[coeff - 1] + dc_diff[coeff];

            if (abs(dc_diff[coeff]) == 0)
                k = 0;
            else if (abs(dc_diff[coeff]) == 1)
                k = 1;
            else if (abs(dc_diff[coeff]) == 2)
                k = 2;
            else
                k = 3;
        }
        bitstring = &bitstring[codeword_size];
    }
    return bitstring;
}

unsigned char* AC_decoding(unsigned char* bitstring, short* run, short* level, int &idx)
{
    int iterator = 0;
    int q, codeword_size, abs_level_minus_1, sign, N;
    int previousRun = 0;
    int previousLevel = 0;
    auto codebook_run = define_codebook_run(previousRun);
    auto codebook_level = define_codebook_level(previousLevel);

    while (true)
    {
        //TODO: Replace this with a more proper way to break
        int cnt = 0;
        for (int i = 0; i < strlen((char*)bitstring); i++)
        {
            if (bitstring[i] == '1')
            {
                cnt = cnt + 1;
            }
        }
        if (cnt == 0)
            break;

        if (iterator == 0)
        {
            q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
            previousRun = 4;
            codebook_run = define_codebook_run(previousRun);
            codeword_size = 2 * q + 1 + codebook_run.Krice;
            
            unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
            for (int i = 0; i < codeword_size; i++)
                tmp[i] = bitstring[i];
            
            run[iterator] = Exp_golomb_decoding(tmp, codebook_run.Kexp);
            bitstring = &bitstring[codeword_size];

            q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
            previousLevel = 2;
            previousLevel = abs(previousLevel) - 1;
            codebook_level = define_codebook_level(previousLevel);

            if (q <= codebook_level.lastRiceQ)
            {
                codeword_size = q + 1 + codebook_level.Krice;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                abs_level_minus_1 = Golomb_decoding(tmp, codebook_level.Krice, codeword_size);
                sign = int(bitstring[codeword_size]) - 48;
                level[iterator] = (abs_level_minus_1 + 1) * (1 - 2 * sign);
                bitstring = &bitstring[codeword_size + 1];
                iterator = iterator + 1;
            }
            else
            {
                bitstring = &bitstring[codebook_level.lastRiceQ + 1];
                q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
                codeword_size = 2 * q + 1 + codebook_level.Kexp;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                N = Exp_golomb_decoding(tmp, codebook_level.Kexp);
                abs_level_minus_1 = N + ((codebook_level.lastRiceQ + 1) * pow(2, codebook_level.Krice));
                sign = int(bitstring[codeword_size]) - 48;
                level[iterator] = (abs_level_minus_1 + 1) * (1 - 2 * sign);
                bitstring = &bitstring[codeword_size + 1];
                iterator = iterator + 1;
            }
        }
        else
        {
            q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
            previousRun = run[iterator - 1];
            codebook_run = define_codebook_run(previousRun);

            if (codebook_run.codebook == "exp")
            {
                //printf("------- WE ENTER EXP FOR RUN\n");
                codeword_size = 2 * q + 1 + codebook_run.Kexp;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                run[iterator] = Exp_golomb_decoding(tmp, codebook_run.Kexp);
                //printf("        --> run %d : %d\n", iterator, run[iterator]);
                bitstring = &bitstring[codeword_size];
            }
            else if (codebook_run.codebook == "combo")
            {
                if (q <= codebook_run.lastRiceQ)
                {
                    //printf("------- WE ENTER COMBO SMALL FOR RUN\n");
                    codeword_size = q + 1 + codebook_run.Krice;
                    
                    unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                    for (int i = 0; i < codeword_size; i++)
                        tmp[i] = bitstring[i];
                    
                    run[iterator] = Golomb_decoding(tmp, codebook_run.Krice, codeword_size);
                    //printf("        --> run %d : %d\n", iterator, run[iterator]);
                    bitstring = &bitstring[codeword_size];
                }
                else
                {
                    //printf("------- WE ENTER COMBO SMALL FOR RUN\n");
                    bitstring = &bitstring[codebook_run.lastRiceQ + 1];
                    q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
                    codeword_size = 2 * q + 1 + codebook_run.Kexp;
                    
                    unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                    for (int i = 0; i < codeword_size; i++)
                        tmp[i] = bitstring[i];
                    
                    N = Exp_golomb_decoding(tmp, codebook_run.Kexp);
                    run[iterator] = N + ((codebook_run.lastRiceQ + 1) * pow(2, codebook_run.Krice));
                    //printf("        --> run %d : %d\n", iterator, run[iterator]);
                    bitstring = &bitstring[codeword_size];
                }
            }
            q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
            previousLevel = level[iterator - 1];
            previousLevel = abs(previousLevel) - 1;
            codebook_level = define_codebook_level(previousLevel);

            if (previousLevel >= 8)
            {
                codebook_level.Kexp = 2;
                codebook_level.codebook = "exp";
            }

            if (codebook_level.codebook == "exp")
            {
                //printf("------- WE ENTER EXP FOR LEVEL\n");
                codeword_size = 2 * q + codebook_level.Kexp + 1;
                
                unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                for (int i = 0; i < codeword_size; i++)
                    tmp[i] = bitstring[i];
                
                abs_level_minus_1 = Exp_golomb_decoding(tmp, codebook_level.Kexp);
                sign = int(bitstring[codeword_size]) - 48;
                level[iterator] = (abs_level_minus_1 + 1) * (1 - 2 * sign);
                //printf("        --> level %d : %d\n", iterator, level[iterator]);
                bitstring = &bitstring[codeword_size + 1];
                iterator = iterator + 1;
            }
            else if (codebook_level.codebook == "combo")
            {
                if (q <= codebook_level.lastRiceQ)
                {
                    //printf("------- WE ENTER COMBO SMALL FOR LEVEL\n");
                    codeword_size = q + 1 + codebook_level.Krice;
                    
                    unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                    for (int i = 0; i < codeword_size; i++)
                        tmp[i] = bitstring[i];
                    
                    abs_level_minus_1 = Golomb_decoding(tmp, codebook_level.Krice, codeword_size);
                    sign = int(bitstring[codeword_size]) - 48;
                    level[iterator] = (abs_level_minus_1 + 1) * (1 - 2 * sign);
                    //printf("        --> level %d : %d\n", iterator, level[iterator]);
                    bitstring = &bitstring[codeword_size + 1];
                    iterator = iterator + 1;
                }
                else
                {
                    //printf("------- WE ENTER COMBO BIG FOR LEVEL\n");
                    bitstring = &bitstring[codebook_level.lastRiceQ + 1];
                    q = std::distance(bitstring, std::find(bitstring, bitstring + strlen((char*)bitstring), '1'));
                    codeword_size = 2 * q + 1 + codebook_level.Kexp;
                    
                    unsigned char* tmp = (unsigned char*)malloc(sizeof(unsigned char) * codeword_size);
                    for (int i = 0; i < codeword_size; i++)
                        tmp[i] = bitstring[i];
                    
                    N = Exp_golomb_decoding(tmp, codebook_level.Kexp);
                    abs_level_minus_1 = N + ((codebook_level.lastRiceQ + 1) * pow(2, codebook_level.Krice));
                    sign = int(bitstring[codeword_size]) - 48;
                    level[iterator] = (abs_level_minus_1 + 1) * (1 - 2 * sign);
                    //printf("        --> level %d : %d\n", iterator, level[iterator]);
                    bitstring = &bitstring[codeword_size + 1];
                    iterator = iterator + 1;
                }
            }
        }
    }
    idx = iterator;
    return bitstring;
}